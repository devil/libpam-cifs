/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/cleanup.h,v 1.2 2007/08/23 07:10:58 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs.h"

// if you intend to use c_malloc(), then place 
// #define USE_C_MALLOC
// before including this file

// if you intend to use c_add(), then place 
// #define USE_C_ADD
// before including this file

#ifdef USE_C_MALLOC
#ifndef USE_C_ADD
#define USE_C_ADD
#endif
#endif

static int isInitialized = 0;

static void (*lcl)() = NULL;

static void **mlist = NULL;
static int  n_mlist = 0;

static void c_free();

static int init(int (*localinit)(), void (*localcleanup)()) {
    if (isInitialized == 0) {
	lcl = localcleanup;
	if (localinit != NULL) {
	    if (localinit() < 0) {
		return -1;
	    }
	}
	isInitialized = 1;
    }
    return 0;
}

#ifdef USE_C_CLEANUP
static int cleanup(int error) {
    int errno_save = errno;

    if (isInitialized > 0) {
	if (lcl != NULL) {
	    lcl();
	}
    }
    c_free();
    isInitialized = 0;
    errno = errno_save;
    return error;
}
#endif

#ifdef USE_C_CLEANUP_P
static void* cleanup_p(void* error_p) {
    int errno_save = errno;

    if (isInitialized > 0) {
	if (lcl != NULL) {
	    lcl();
	}
    }
    c_free();
    isInitialized = 0;
    errno = errno_save;
    return error_p;
}
#endif

static void c_free() {
    if (mlist != NULL) {
	for(int i = 0; i < n_mlist; i += 1) {
	    if (mlist[i] != NULL) {
		free(mlist[i]);
	    }
	}
	free(mlist);
	mlist = NULL;
	n_mlist = 0;
    }
}

#ifdef USE_C_ADD
static void *c_add(void *ptr) {
    mlist = (void **) realloc(mlist, (n_mlist + 1) * sizeof(void **));
    mlist[n_mlist] = ptr;
    n_mlist += 1;
    return ptr;
}
#endif

#ifdef USE_C_MALLOC
static void *c_malloc(size_t size) {
    return c_add(malloc(size));
}
#endif
