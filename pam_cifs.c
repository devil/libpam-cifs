/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/pam_cifs.c,v 1.6 2007/12/15 23:09:24 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs_module.h"
#include "cifsumountd.h"

extern struct options pam_cifs_options;

void pam_cifs_pamh_cleanup(pam_handle_t *pamh, void* data, int pam_end_status) {
    if (data != NULL) {
	free(data);
    }
}

PAM_EXTERN
int pam_sm_authenticate(pam_handle_t *pamh,int flags,int argc ,const char **argv) {
    // this function does not do any authentication itself. The only
    // purpose is to get the authentication token, since this is
    // needed for mounting the network-shares in the open_session
    // part.
    // If we can get the authtok from the pam, all is o.k. If we
    // can't, we start a conversation.
    // After getting the authtok, we store it (and free it after
    // successfully opened session) as PAM_CIFS_AUTHTOK.
    int retval = PAM_SUCCESS;
    char *authtok = NULL;

    slog_init("pam_cifs");

    parse_options(argc, argv);
    
    if ((retval = pam_get_item(pamh, PAM_AUTHTOK, (const void **)&authtok)) != PAM_SUCCESS) {
	slog("authtok: %s\n", pam_strerror(pamh, retval));
	return retval;
    }
    if (authtok != NULL) {
	slog("authtok ok %\n");
	// since we use strdup() we have to free it in the
	// open_session section
	if ((retval = pam_set_data(pamh, PAM_CIFS_AUTHTOK,
				   strdup(authtok), NULL)) != PAM_SUCCESS) {
	    slog("pam_set_item: %s\n", pam_strerror(pamh, retval));
	    return retval;
	}
    }
    else {
	slog("authtok nok, trying conversation\n");

	struct pam_conv *conv;
	if ((retval = pam_get_item(pamh, PAM_CONV, (const void **) &conv)) != PAM_SUCCESS) {
	    slog("pam_get_item: %s\n", pam_strerror(pamh, retval));
	    return retval;
	}

	if (conv != NULL) {
	    slog("conv ok %\n");
	}
	else {
	    slog("conv nok %\n");
	    return PAM_CONV_ERR;
	}
	
	struct pam_message msg[1], *mesg[1];
	mesg[0] = &msg[0];
	msg[0].msg_style = PAM_PROMPT_ECHO_OFF;
	msg[0].msg = "Password: ";
	struct pam_response *resp = NULL;

	if ((retval = conv->conv(1, (const struct pam_message **)mesg, &resp,
				 conv->appdata_ptr)) != PAM_SUCCESS) {
	    slog("conversation: %s\n", pam_strerror(pamh, retval));
	    return PAM_CONV_ERR;
	    
	}

	if ((resp != NULL) && (resp[0].resp != NULL)) {
	    if ((retval = pam_set_item(pamh, PAM_AUTHTOK, resp[0].resp)) != PAM_SUCCESS) {
		slog("setting authtok: %s\n", pam_strerror(pamh, retval));
		return retval;
	    }
	    // cleanup needed
	    if ((retval = pam_set_data(pamh, PAM_CIFS_AUTHTOK,
				       strdup(resp[0].resp), pam_cifs_pamh_cleanup)) != PAM_SUCCESS) {
		slog("pam_set_item: %s\n", pam_strerror(pamh, retval));
		return retval;
	    }
	    free(resp);
	}
	else {
	    return PAM_CONV_ERR;
	}
    }

    return PAM_SUCCESS;	
}

PAM_EXTERN
int pam_sm_setcred(pam_handle_t *pamh, int flags, int argc, const char **argv){
    return PAM_SUCCESS;
}

PAM_EXTERN
int pam_sm_close_session(pam_handle_t *pamh, int flags, int argc, const char **argv){

    parse_options(argc, argv);

    // We have to find the cifsumountd-daemon and it's socket to
    // trigger. We can't send a SIGHUP-signal, because this function
    // is run as unpriveleged user (normally) and cifsumountd runs as
    // root.
    char pidfilename[PATH_MAX];
    snprintf(pidfilename, PATH_MAX, "%s/%s", VARUN, PIDFILE);
    FILE *pidfile;
    if ((pidfile = fopen(pidfilename, "r")) == NULL) {
	slog("Cant open pidfile %s", pidfilename);
	return PAM_SESSION_ERR;
    }
    char pida[NAME_MAX];
    if (fgets(pida, NAME_MAX, pidfile) == NULL) {
	slog("Cant read pid from pidfile %s", pidfilename);
	return PAM_SESSION_ERR;
    }
    fclose(pidfile);
    pid_t pid = atoi(pida);

    slog("found cifsmountd pid=%d\n", pid);
    
    int sock;
    if ((sock = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
	slog("Cant create socket: %s", strerror(errno));
	return PAM_SESSION_ERR;
    }
    
    struct sockaddr_un saddr;
    saddr.sun_family = AF_UNIX;
    snprintf(saddr.sun_path, UNIX_PATH_MAX, "%s.%d", SOCKET, (int) pid);

    char *msg = "t";
    if (sendto(sock, (const void *)msg, strlen(msg), 0,
	       (const struct sockaddr *) &saddr, sizeof(saddr)) < 0) {
	slog("Cant trigger daemon: %s", strerror(errno));
	return PAM_SESSION_ERR;
    }
    slog("sent trigger to cifsumountd\n");
    
    return PAM_SUCCESS;
}

#ifdef PAM_STATIC
struct pam_module _pam_cifs_modstruct = {
    "pam_cifs",
    pam_sm_authenticate,
    pam_sm_setcred,
    NULL,
    pam_sm_open_session,
    pam_sm_close_session,
    NULL
};
#endif

