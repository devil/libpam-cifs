/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/pam_cifs.h,v 1.10 2007/09/25 12:04:41 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef PAM_CIFS_H
#define PAM_CIFS_H PAM_CIFS_H

#include "common.h"

#define MOUNTS "/proc/mounts"

#define BUFFER_SIZE 1024
#define MAX_MTAB 100

#ifndef DEFAULT_PATH_PREFIX
#define DEFAULT_PATH_PREFIX "undefined"
#endif
#ifndef DEFAULT_PATH_SUFFIX
#define DEFAULT_PATH_SUFFIX "undefined"
#endif
#ifndef DEFAULT_SOURCE_PREFIX
#define DEFAULT_SOURCE_PREFIX "undefined"
#endif
#ifndef DEFAULT_SOURCE_SUFFIX
#define DEFAULT_SOURCE_SUFFIX ""
#endif
#ifndef DEFAULT_MOUNTER
#define DEFAULT_MOUNTER "/sbin/mount.cifs"
#endif
#ifndef DEFAULT_DOMAIN
#define DEFAULT_DOMAIN "undefined"
#endif
#ifndef DEFAULT_OPTIONS
#define DEFAULT_OPTIONS ""
#endif
#ifndef DEFAULT_MIN_UID
#define DEFAULT_MIN_UID 1000
#endif
#ifndef DEFAULT_MAX_UID
#define DEFAULT_MAX_UID 65000
#endif
#ifndef DEFAULT_LINK_NAME
#define DEFAULT_LINK_NAME ""
#endif
#ifndef DEFAULT_BACKGROUND
#define DEFAULT_BACKGROUND 1
#endif
#ifndef DEFAULT_OBJECTCLASS
#define DEFAULT_OBJECTCLASS "Users"
#endif
#ifndef DEFAULT_NOUSERINSOURCE
#define DEFAULT_NOUSERINSOURCE 0
#endif

#define CIFS_STRING   "cifs"
#define CIFS_USERNAME "username"
#define CIFS_ODELIM   ","
#define CIFS_VDELIM   "="

#define LDAP_OPTIONS_PREFIX "ldap:"

struct options {
    // mount-point will be <pathprefix>/<user><pathsuffix>
    char pathprefix[PATH_MAX];
    char pathsuffix[NAME_MAX + 1];
    // to include debug messages, maybe security hole
    int debug;
    // unc of source to mount
    // source will be <sourceprefix>/<user><sourcesuffix>
    char sourceprefix[PATH_MAX];
    char sourcesuffix[NAME_MAX + 1];
    // only for uid's in the range [min_uid,max_uid] the mount will be
    // performed. This is to exclude root, operator and others.
    uid_t min_uid;
    uid_t max_uid;
    // To authenticate towards servers that are Active Directory
    // members, mount.cifs often needs to send the DOMAIN mount
    // parameter.  Put that value in this option.  If the string
    // is empty, only the username will be sent.
    char domain[NAME_MAX + 1];
    // General options to mount.cifs, comma-separated exactly as they
    // would be given behind the -o option to mount.cifs
    char options[NAME_MAX + 1];
    // name of a link to be placed in the users HOME-directory to
    // point to the mount-point. If it is there, it will NOT be
    // overridden. If the string is empty (default), no link will be
    // created 
    char linkname[NAME_MAX + 1];
    // if make_mount_point == 1, then the mount point will be
    // created. In detail: the <pathprefix> must exist and the pathcomponent
    // <user><pathsuffix> will be created.
    int make_mount_point;
    // if mount_home == 1 the directory being attempted mounted will
    // be the user's home directory.  Does not currently work with
    // make_mount_point, you need to use pam_mkhomedir for that.
    // sourceprefix must be set to first path element (regexp) of the
    // home directories for cifsumountd to work with this option.
    int mount_home;
    // if background == 0 the pam_cifs module will wait until the successful
    // completion of the mount.cifs call before returning with success.
    // if background == 1 the pam_cifs module will background the mount
    // process and return with success before the mount.cifs call finishes.
    // The latter is faster, the former is necessary if you need to be sure
    // the mount process has finished before going to the next step in the 
    // pam stack.
    int background;
    // the uri of the ldap-server
    char ldap[NAME_MAX + 1];
    // basedn for searching the ldap-server
    char ldapbasedn[NAME_MAX + 1];
    // binddn for connecting the ldap-server
    char ldapbinddn[NAME_MAX + 1];
    // password fpr binddn 
    char ldapbindpw[NAME_MAX + 1];
    // attribute which contains the account-name to find the user
    char ldaploginattribute[NAME_MAX + 1];
    // objectclass for the user-objects
    char ldapobjectclass[NAME_MAX + 1];
    // whether or not to use the user-name in the source uri
    int nouserinsource;
    // whether or not to use local user uid
    int unix;
};

typedef struct mtab mtab_t;
struct mtab {
    char source[PATH_MAX];
    char target[PATH_MAX];
    char type[PATH_MAX];
    char options[PATH_MAX];
};

int pexec(const char *cmdpath, char *argv[], int background, char *input, char *output, size_t noutput);
void parse_options(int argc, const char **argv);
int do_mount(char *source, char *target, char *domain, char *options, const char *authtok, uid_t uid, gid_t gid, const char *user, int background);
int already_mounted(char *path);
int read_mounts(mtab_t *mtab, size_t size);
void slog(const char *format, ...);
void slog_init(const char *string);
int mkdirpath(const char*, const char*, mode_t mode, uid_t uid, gid_t gid);

#endif
