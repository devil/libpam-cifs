/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/mkdirpath.c,v 1.3 2007/08/23 07:11:21 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs.h"
#define USE_C_ADD
#define USE_C_CLEANUP
#include "cleanup.h"

int
mkdirpath(const char* prefix, const char* newpath, mode_t mode, uid_t uid, gid_t gid) {
    if (init(NULL, NULL) < 0) {
	return cleanup(-1);
    }
    
    if (prefix == NULL) {
	return cleanup(-1);
    }
    if (newpath == NULL) {
	return cleanup(-1);
    }

    struct stat statbuf;

    if (stat(prefix, &statbuf) < 0) {
	slog("stat: %s\n", strerror(errno));
	cleanup(-1);
    }
    if (! S_ISDIR(statbuf.st_mode)) {
	slog("%s is not a directory\n", prefix);
	cleanup(-1);
    }

    char newdir[PATH_MAX];
    strncpy(newdir, prefix, PATH_MAX);

    char* npath = c_add(strdup(newpath));

    char* component = NULL;
    char* npath2 = NULL;
    while((component = strtok_r(npath, "/", &npath2)) != NULL) {
	npath = NULL;

	strncat(newdir, "/", sizeof(newdir)-strlen(newdir)-1);
	strncat(newdir, component, sizeof(newdir)-strlen(newdir)-1);

	if (stat(newdir, &statbuf) < 0) {
	    if (errno == ENOENT) {
		slog("creating %s\n", newdir);
	
		if (mkdir(newdir, mode) < 0) {
		    slog("mkdir: %s\n", strerror(errno));
		    return cleanup(-1);
		}
		if (chown(newdir, uid, gid) < 0) {
		    slog("chown: %s\n", strerror(errno));
		    return cleanup(-1);
		}
	    }
	    else {
		slog("stat: %s\n", strerror(errno));
		cleanup(-1);
	    }
	}
	
    }
    
    return cleanup(0);
}
