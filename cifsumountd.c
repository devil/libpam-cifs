/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/cifsumountd.c,v 1.5 2005/08/29 11:33:42 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs.h"
#include "cifsumountd.h"
#include <sys/mount.h>

struct options pam_cifs_options;

static int sock;
static struct sockaddr_un saddr;

int user_process(uid_t uid) {
    // check to see if we have a user process with this uid.
    // check all entries in the /proc-directory
    DIR *d = opendir(PROC);
    if (d == NULL) {
	return -1;
    }
    int count = 0;
    struct dirent *dentry;
    while ((dentry = readdir(d)) != NULL) {
	if ((dentry->d_name[0] >= '1') && (dentry->d_name[0] <= '9')) {
	    // only entries which begin with a number are of interest 
	    char p[PATH_MAX];
	    snprintf(p, PATH_MAX, "%s/%s", PROC, dentry->d_name);
	    struct stat statbuf;
	    if (stat(p, &statbuf) < 0) {
		slog("stat: %s", strerror(errno));
	    }
	    else {
		if (S_ISDIR(statbuf.st_mode)) {
		    if (statbuf.st_uid == uid) {
			count += 1;
		    }
		}
	    }
	}
    }
    closedir(d);
    // return the number of entries we found
    return count;
}


void
sigHUPhandler(int signal) {
    slog("triggered");
}

void
sigTERMhandler(int signal) {
    if (close(sock) < 0) {
	slog("Can't close socket: %s", saddr.sun_path);
    }
    if (unlink(saddr.sun_path) < 0) {
	slog("Can't unlink socket: %s", saddr.sun_path);
    }
    slog("terminated");
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
    saddr.sun_path[0]='\0';

    struct sigaction sa;

    // SIGHUP is used to enforced a check if something can be
    // unmounted 
    sa.sa_handler = sigHUPhandler;
    sa.sa_flags = 0;
    sa.sa_flags |= SA_RESTART;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGHUP, &sa, NULL) < 0) {
	perror("sigaction");
	exit(EXIT_FAILURE);
    }
    // SIGTERM-handler for normal termination
    sa.sa_handler = sigTERMhandler;
    sa.sa_flags = 0;
    sa.sa_flags |= SA_RESTART;
    sigemptyset(&sa.sa_mask);
    if (sigaction(SIGTERM, &sa, NULL) < 0) {
	perror("sigaction");
	exit(EXIT_FAILURE);
    }
    
    slog_init("cifsmountd");

    parse_options(argc, (const char **)argv);

    daemonize();

    if ((sock = socket(AF_UNIX, SOCK_DGRAM, 0)) < 0) {
	slog("Cant create socket: %s, exiting ...", strerror(errno));
	exit(EXIT_FAILURE);
    }
    saddr.sun_family = AF_UNIX;
    snprintf(saddr.sun_path, UNIX_PATH_MAX, "%s.%d", SOCKET, (int)getpid());
    if (bind(sock, (struct sockaddr *)&saddr, (socklen_t)sizeof(saddr)) < 0) {
	slog("Cant bind socket: %s, exiting ...", strerror(errno));
	exit(EXIT_FAILURE);
    }
    if (chmod(saddr.sun_path, S_IRWXU | S_IRWXG | S_IRWXO ) < 0) {
	slog("Cant chmod socket: %s, exiting ...", strerror(errno));
	exit(EXIT_FAILURE);
    }

    fd_set fdset;
    
    char line[PATH_MAX];
    char pidfilename[PATH_MAX];
    snprintf(pidfilename, PATH_MAX, "%s/%s", VARUN, PIDFILE);
    snprintf(line, PATH_MAX, "echo %d > %s", (int)getpid(), pidfilename);
    system(line);
    if (chmod(pidfilename, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH) < 0) {
	slog("Can't chmod pidfile: %s", strerror(errno));
    }
        
    slog("%s start", argv[0]);
    while(1) {
	slog("%s round ...", argv[0]);
	cifs_check();

	struct timeval tv;
	tv.tv_sec = SECONDSTOWAIT;
	tv.tv_usec = 0;
	FD_ZERO(&fdset);
	FD_SET(sock, &fdset);
	int ret;
	if ((ret = select(sock + 1, &fdset, NULL, NULL, &tv)) < 0) {
	    if (errno != EINTR) {
		slog("select: %s", strerror(errno));
	    }
	}
	else {
	    if ((ret > 0) && (FD_ISSET(sock, &fdset))) {
		int nread;
		char msg[LINE_MAX];
		if ((nread = recvfrom(sock, msg, LINE_MAX, 0, NULL, 0)) < 0) {
		    slog("recvfrom: %s", strerror(errno));
		}
		else {
		    msg[nread] = '\0';
		    slog("got: %s", msg);
		    sleep(3); // wait processes to vanish
		}
	    }
	    else {
		slog("time elapsed");
	    }
	}
    }
}

