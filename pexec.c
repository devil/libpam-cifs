/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/pexec.c,v 1.7 2006/08/01 15:02:18 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs.h"

static int  down_fd[2];
static int  up_fd[2];

static int isInitialized = 0;

static int init() {
    if (isInitialized == 0) {
	if (pipe(down_fd) < 0) {
	    slog("pipe: %s", strerror(errno));
	    return -1;
	}	
	if (pipe(up_fd) < 0) {
	    slog("pipe: %s", strerror(errno));
	    return -1;
	}
	isInitialized = 1;
    }
    return 0;
}

static int cleanup(int error) {
    int errno_save = errno;
    if (isInitialized > 0) {
	close(down_fd[PIPE_WRITE]);
	close(down_fd[PIPE_READ]);
	close(up_fd[PIPE_WRITE]);
	close(up_fd[PIPE_READ]);
    }
    isInitialized = 0;
    errno = errno_save;
    return error;
}

int
pexec(const char *cmdpath, char *argv[], int background, char *input, char *output, size_t noutput) {
    pid_t  cpid;
    if (init() < 0) {
	return -1;
    }
    
    if ((cpid = fork()) == 0) { // Child
	if (dup2(down_fd[PIPE_READ], STDIN_FILENO) < 0) {
	    slog("child:dup2: %s", strerror(errno));
	    exit(EXIT_FAILURE);
	}
	if (dup2(up_fd[PIPE_WRITE], STDOUT_FILENO) < 0) {
	    slog("child:dup2: %s", strerror(errno));
	    exit(EXIT_FAILURE);
	}
	if (dup2(up_fd[PIPE_WRITE], STDERR_FILENO) < 0) {
	    slog("child:dup2: %s", strerror(errno));
	    exit(EXIT_FAILURE);
	}
	if (close(down_fd[PIPE_WRITE]) < 0) {
	    slog("child:close: %s", strerror(errno));
	    exit(EXIT_FAILURE);
	}
	if (close(up_fd[PIPE_READ]) < 0) {
	    slog("child:close: %s", strerror(errno));
	    exit(EXIT_FAILURE);
	}

	if (setsid() <0) {
	    slog("child:setsid: %s", strerror(errno));
	    exit(EXIT_FAILURE);	    
	}
	// deamonize ?
	if (background == 1) {
            if ((cpid = fork()) < 0) {
	        slog("fork: %s", strerror(errno));
	        exit(EXIT_FAILURE);
	    }
	    else if (cpid > 0) { // parent
	        exit(EXIT_SUCCESS);
	    }
	}
	// child-child if background, child otherwise
	slog("exec: %s", cmdpath);
	if (execvp(cmdpath, argv) < 0) {
	    slog("child:exec: %s", strerror(errno));
	    exit(EXIT_FAILURE);
	}
	
	exit(EXIT_SUCCESS); // only reached if not background
    }
    else if (cpid > 0) { // Parent
	if (close(down_fd[PIPE_READ]) < 0) {
	    slog("close: %s", strerror(errno));
	    return cleanup(-1);
	}
	if (close(up_fd[PIPE_WRITE]) < 0) {
	    slog("close: %s", strerror(errno));
	    return cleanup(-1);
	}

	int nread;
	if ((nread = read(up_fd[PIPE_READ], output, noutput)) < 0) {
	    slog("read: %s", strerror(errno));
	    return cleanup(-1);
	}
	output[nread] = '\0';
	slog("got form child: %s", output); 

	if (nread > 0) {
	    int nwritten;
	    if ((nwritten = write(down_fd[PIPE_WRITE], input, strlen(input))) < 0) {
		if (errno != EPIPE) {
		    slog("write: %s", strerror(errno));
		}
	    }
	    else {
		slog("written: %d", nwritten);
	    }
	}
	int status;
	if (waitpid(cpid, &status, 0) < 0) {
	    slog("waitpid: %s", strerror(errno));
	}
	if (WIFEXITED(status)) {
	    slog("exited with: %d\n", WEXITSTATUS(status));
	}
	if (WEXITSTATUS(status) != 0) {
	    return cleanup(-1);
	}
    }
    else { // fork error
	slog("fork: %s", strerror(errno));
	return cleanup(-1);
    }
    return cleanup(0);
}


