/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/parse_options.c,v 1.10 2007/09/25 12:04:41 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs.h"

// for a description of the options see pam_cifs.h

struct options pam_cifs_options;

void
parse_options(int argc, const char **argv) {
    // set the default values
    pam_cifs_options.max_uid = DEFAULT_MAX_UID;
    pam_cifs_options.min_uid = DEFAULT_MIN_UID;
    strncpy(pam_cifs_options.sourcesuffix, DEFAULT_SOURCE_SUFFIX, NAME_MAX);
    strncpy(pam_cifs_options.sourceprefix, DEFAULT_SOURCE_PREFIX, PATH_MAX);
    strncpy(pam_cifs_options.pathsuffix, DEFAULT_PATH_SUFFIX, NAME_MAX);
    strncpy(pam_cifs_options.pathprefix, DEFAULT_PATH_PREFIX, PATH_MAX);
    strncpy(pam_cifs_options.domain, DEFAULT_DOMAIN, NAME_MAX);
    strncpy(pam_cifs_options.options, DEFAULT_OPTIONS, NAME_MAX);
    strncpy(pam_cifs_options.linkname, DEFAULT_LINK_NAME, NAME_MAX);
    pam_cifs_options.debug = 0;
    pam_cifs_options.make_mount_point = 0;
    pam_cifs_options.mount_home = 0;
    pam_cifs_options.background = DEFAULT_BACKGROUND;
    pam_cifs_options.nouserinsource = DEFAULT_NOUSERINSOURCE;
    pam_cifs_options.unix = 0;
    
    strncpy(pam_cifs_options.ldap, "", NAME_MAX);
    strncpy(pam_cifs_options.ldapbasedn, "", NAME_MAX);
    strncpy(pam_cifs_options.ldapbinddn, "", NAME_MAX);
    strncpy(pam_cifs_options.ldapbindpw, "", NAME_MAX);
    strncpy(pam_cifs_options.ldaploginattribute, "", NAME_MAX);
    strncpy(pam_cifs_options.ldapobjectclass, DEFAULT_OBJECTCLASS, NAME_MAX);

    // parse the command line
    for(int i = 0; i < argc; i += 1) {
	char *o = strdup(argv[i]);
	if (o == NULL) {
	    return;
	}
	slog("arg=[%s]\n", o);

		char *param = o;
	char *value;
	
	char *ind = index(o, '=');
	if (ind == NULL) {
	    value = NULL;
	}
	else {
	    *ind = '\0';
	    value = ind + 1;	    
	}

	if (value != NULL) {
	    slog("param=%s, value=%s\n", param, value);
	}
	
	if (strcmp(param, "debug") == 0) {
	    pam_cifs_options.debug = 1;
	}

	if (strcmp(param, "make_mount_point") == 0) {
	    pam_cifs_options.make_mount_point = 1;
	}

	if (strcmp(param, "mount_home") == 0) {
	    pam_cifs_options.mount_home = 1;
	}

	if (strcmp(param, "prefix") == 0) {
	    if (value != NULL) {
		strncpy(pam_cifs_options.pathprefix, value, PATH_MAX);
	    }
	}

	if (strcmp(param, "suffix") == 0) {
	    if (value != NULL) {
		strncpy(pam_cifs_options.pathsuffix, value, NAME_MAX);
	    }
	}

	if (strcmp(param, "source") == 0) {
	    if (value != NULL) {
		strncpy(pam_cifs_options.sourceprefix, value, PATH_MAX);
	    }
	}

    	if (strcmp(param, "ssuffix") == 0) {
	    if (value != NULL) {
		strncpy(pam_cifs_options.sourcesuffix, value, NAME_MAX);
	    }
	}

	if (strcmp(param, "windomain") == 0) {
	    if (value != NULL) {
                strncpy(pam_cifs_options.domain, value, NAME_MAX);
	    }
	}

	if (strcmp(param, "options") == 0) {
	    if (value != NULL) {
                if (pam_cifs_options.options != NULL) {
   		   strncat(pam_cifs_options.options, value, sizeof(pam_cifs_options.options)-strlen(pam_cifs_options.options)-1);
                }
		else {
		   strncpy(pam_cifs_options.options, value, NAME_MAX);
                }
	    }
	}

	if (strcmp(param, "linkname") == 0) {
	    if (value != NULL) {

		strncpy(pam_cifs_options.linkname, value, NAME_MAX);
	    }
	}

    	if (strcmp(param, "min_uid") == 0) {
	    if (value != NULL) {
		pam_cifs_options.min_uid = atoi(value);
	    }
	}
    	if (strcmp(param, "max_uid") == 0) {
	    if (value != NULL) {
		pam_cifs_options.min_uid = atoi(value);
	    }
	}
	if (strcmp(param, "background") == 0) {
	    if (value != NULL) {
	         pam_cifs_options.background = atoi(value);
	    }
	}
	if (strcmp(param, "ldap") == 0) {
	    if (value != NULL) {
		strncpy(pam_cifs_options.ldap, value, NAME_MAX);
	    }
	}
	if (strcmp(param, "ldapbasedn") == 0) {
	    if (value != NULL) {
		strncpy(pam_cifs_options.ldapbasedn, value, NAME_MAX);
	    }
	}
	if (strcmp(param, "ldapbinddn") == 0) {
	    if (value != NULL) {
		strncpy(pam_cifs_options.ldapbinddn, value, NAME_MAX);
	    }
	}
	if (strcmp(param, "ldapbindpw") == 0) {
	    if (value != NULL) {
		strncpy(pam_cifs_options.ldapbindpw, value, NAME_MAX);
	    }
	}
	if (strcmp(param, "ldapbasedn") == 0) {
	    if (value != NULL) {
		strncpy(pam_cifs_options.ldapbasedn, value, NAME_MAX);
	    }
	}
	if (strcmp(param, "ldaploginattribute") == 0) {
	    if (value != NULL) {
		strncpy(pam_cifs_options.ldaploginattribute, value, NAME_MAX);
	    }
	}
	if (strcmp(param, "ldapobjectclass") == 0) {
	    if (value != NULL) {
		strncpy(pam_cifs_options.ldapobjectclass, value, NAME_MAX);
	    }
	}
	if (strcmp(param, "nouserinsource") == 0) {
	    if (value != NULL) {
	         pam_cifs_options.nouserinsource = atoi(value);
	    }
	}
	if (strcmp(param, "unix") == 0) {
	    if (value != NULL) {
	         pam_cifs_options.unix = atoi(value);
	    }
	}
    }
    if (pam_cifs_options.debug > 0) {
	slog("pathprefix=%s", pam_cifs_options.pathprefix);
	slog("pathsuffix=%s", pam_cifs_options.pathsuffix);
	slog("sourceprefix=%s", pam_cifs_options.sourceprefix);
	slog("sourcesuffix=%s", pam_cifs_options.sourcesuffix);
	slog("windomain=%s", pam_cifs_options.domain);
	slog("options=%s", pam_cifs_options.options);
	slog("linkname=%s", pam_cifs_options.linkname);
	slog("min_uid=%d", pam_cifs_options.min_uid);
	slog("max_uid=%d", pam_cifs_options.max_uid);
	slog("make_mount_point=%d", pam_cifs_options.make_mount_point);
	slog("mount_home=%d", pam_cifs_options.mount_home);
	slog("background=%d", pam_cifs_options.background);
	slog("ldap=%s", pam_cifs_options.ldap);
	slog("ldapbasedn=%s", pam_cifs_options.ldapbasedn);
	slog("ldapbinddn=%s", pam_cifs_options.ldapbinddn);
	slog("ldapbindpw=%s", pam_cifs_options.ldapbindpw);
	slog("ldaploginattribue=%s", pam_cifs_options.ldaploginattribute);
	slog("ldapobjectclass=%s", pam_cifs_options.ldapobjectclass);
	slog("nouserinsource=%d", pam_cifs_options.nouserinsource);
	slog("unix=%d", pam_cifs_options.unix);
    }
}

