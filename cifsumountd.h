/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/cifsumountd.h,v 1.2 2005/07/07 07:45:12 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef CIFSUMOUNTD_H
#define CIFSUMOUNTD_H CIFSUMOUNTD_H

#define PROC   "/proc"
#define VARUN  "/var/run"
#define UMOUNT "/bin/umount"
#define PIDFILE "cifsumountd.pid"
#define SOCKET "/tmp/cifsumounts.socket"
#define UNIX_PATH_MAX 108 // man 7 unix
#define MAX_OPTIONS 50
#define SECONDSTOWAIT 3600

typedef struct mopts mopts_t;
struct mopts {
    char key[LINE_MAX];
    char value[LINE_MAX];
};

int user_process(uid_t uid);
int cifs_check();
void daemonize();

char *get_mount_option_value(const char *key, mopts_t *mopts, size_t nopts);
int parse_mount_options(const char *options, mopts_t *mopts, size_t max_options);

#endif
