/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/pam_sm_open_session.c,v 1.17 2007/12/15 23:09:07 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs_module.h"
#define USE_C_CLEANUP
#include "cleanup.h"

extern struct options pam_cifs_options;

static struct sigaction nsa, osa;
//static int needtofreeauthtok = 0;
static char *authtok=NULL;

#ifdef NO_PAM_MODULE
extern char* testuser;
extern char* testauthtok;
#endif

static int localinit() {
    slog_init("pam_cifs");
    nsa.sa_handler = SIG_IGN;
    nsa.sa_flags = 0;
    sigemptyset(&nsa.sa_mask);

    if (sigaction(SIGPIPE, &nsa, &osa) < 0) {
	slog("sigaction: %s", strerror(errno));
	return -1;
    }
    return 0;
}

static void localcleanup() {
    if (sigaction(SIGPIPE, &osa, NULL) < 0) {
	slog("sigaction: %s", strerror(errno));
    }
}

PAM_EXTERN
int pam_sm_open_session(pam_handle_t *pamh, int flags, int argc, const char **argv){
    if (init(localinit, localcleanup) < 0) {
	return PAM_SESSION_ERR;
    }
    
    parse_options(argc, argv);

    int retval;
    const char *user=NULL;

// Putting this first test in until such time as I have time to fix 
// make_mount_point to not assume mount_points have to be subdirectories 
// of the home directory
    if ((pam_cifs_options.make_mount_point == 1) && (pam_cifs_options.mount_home == 1)) {
       slog("Cannot combine make_mount_point and mount_home\n");
       return PAM_SESSION_ERR;
    }

#ifdef NO_PAM_MODULE
    user  = testuser;
#else
    if ((retval = pam_get_user(pamh, &user, NULL)) != PAM_SUCCESS) {
	slog("open_session", pam_strerror(pamh, retval));
	return cleanup(retval);
    }
#endif
    if (user == NULL) {
	slog("no valid user\n");
	return cleanup(PAM_USER_UNKNOWN);
    }
    else {
	slog("user=%s\n", user);
    }
#ifdef NO_PAM_MODULE
    // fake it
    struct passwd p;
    struct passwd *pwd;
    pwd = &p;
    pwd->pw_uid = 4711;
    pwd->pw_gid = 4711;
    pwd->pw_dir = "/tmp";
#else
    struct passwd *pwd = getpwnam(user);
    if (pwd == NULL) {
	slog("can't identify user\n");
	return cleanup(PAM_USER_UNKNOWN);
    }
#endif
    slog("uid=%d", pwd->pw_uid);
    slog("gid=%d", pwd->pw_gid);

    if ((pwd->pw_uid < pam_cifs_options.min_uid) || (pwd->pw_uid > pam_cifs_options.max_uid)) {
	slog("uid out of range\n");
	return cleanup(PAM_SUCCESS);
    }

#ifdef NO_PAM_MODULE
    authtok = testauthtok;
#else
    if ((retval = pam_get_item(pamh, PAM_AUTHTOK, (const void **)&authtok)) != PAM_SUCCESS) {
	slog("open_session: %s\n", pam_strerror(pamh, retval));
	return cleanup(retval);
    }
    if (authtok == NULL) {
	slog("no auth-token from system, trying local ...\n");
	if ((retval = pam_get_data(pamh, PAM_CIFS_AUTHTOK, (const void **)&authtok)) != PAM_SUCCESS) {
	    slog("no auth-token: %s\n", pam_strerror(pamh, retval));
	    return cleanup(PAM_CRED_INSUFFICIENT);
	}
	slog("... got local\n");
    }
#endif
    if (authtok == NULL) {
	slog("no valid auth-token at all\n");
	return cleanup(PAM_CRED_INSUFFICIENT);
    }

    char target[PATH_MAX];
    if (pam_cifs_options.mount_home > 0) {
       strncpy(target, pwd->pw_dir, sizeof(target));
    }
    else {
       strncpy(target, pam_cifs_options.pathprefix, sizeof(target));
       strncat(target, "/", sizeof(target)-strlen(target)-1);
       strncat(target, user, sizeof(target)-strlen(target)-1);
       strncat(target, pam_cifs_options.pathsuffix, sizeof(target)-strlen(target)-1);
    }

    struct stat statbuf;
    if (stat(target, &statbuf) < 0) {
	slog("mount point %s does not exist", target);
	if (pam_cifs_options.make_mount_point > 0) {
	    slog("should create mount point %s", target);
	    char newpath[PATH_MAX];
	    strncpy(newpath, user, sizeof(newpath));
	    strncat(newpath, pam_cifs_options.pathsuffix, sizeof(newpath)-strlen(newpath)-1);
	    if (mkdirpath(pam_cifs_options.pathprefix, newpath, S_IRWXU, pwd->pw_uid, pwd->pw_gid) < 0) {
		slog("can't make %s in %s\n", newpath, pam_cifs_options.pathprefix);
		return cleanup(PAM_SESSION_ERR);
	    }
	}
	else {
	    return cleanup(PAM_SESSION_ERR);
	}
    }

    if ((retval = already_mounted(target)) > 0) {
	return cleanup(PAM_SUCCESS);
    }
    else if (retval < 0) {
	return cleanup(PAM_SESSION_ERR);
    }

    char source[PATH_MAX];

#ifdef WITH_LDAP
    if (strncmp(pam_cifs_options.sourceprefix,
		LDAP_OPTIONS_PREFIX, strlen(LDAP_OPTIONS_PREFIX)) == 0) {
	char* ldapsrc;
	char* attribute = pam_cifs_options.sourceprefix + strlen(LDAP_OPTIONS_PREFIX);
	    
	if ((ldapsrc = ldap_source(pamh, user, attribute)) == NULL) {
	    return cleanup(PAM_SESSION_ERR);
	}
	else {
	    strncpy(source, ldapsrc, sizeof(source));
	}
    }
    else {
	strncpy(source, pam_cifs_options.sourceprefix, sizeof(source));
	if (pam_cifs_options.nouserinsource == 0) {
	    strncat(source, "/", sizeof(source)-strlen(source)-1);
	    strncat(source, user, sizeof(source)-strlen(source)-1);
	}
	strncat(source, pam_cifs_options.sourcesuffix, sizeof(source)-strlen(source)-1);
    }
#else
	strncpy(source, pam_cifs_options.sourceprefix, sizeof(source));
	strncat(source, "/", sizeof(source)-strlen(source)-1);
	if (pam_cifs_options.nouserinsource == 0) {
	    strncat(source, user, sizeof(source)-strlen(source)-1);
	}
	strncat(source, pam_cifs_options.sourcesuffix, sizeof(source)-strlen(source)-1);
#endif
    
    char strippedpathcomponents[PATH_MAX + 1] = "";;
    char *s;
    do {
	if (strlen(source) <= 2) {
	    slog("malformed share uri: %s\n", source);
	    return cleanup(PAM_SESSION_ERR);
	}
	slog("try to mount share: %s\n", source);

	int mount_count = 1;
	int mount_error = -1;
	do {
	    slog("attempt %d\n", mount_count);
	    if (pam_cifs_options.unix == 0) {
	       mount_error = do_mount(source, target, pam_cifs_options.domain,
				   pam_cifs_options.options, authtok, pwd->pw_uid,
				   pwd->pw_gid, user, pam_cifs_options.background);
	    }
	    else {
	       mount_error = do_mount(source, target, pam_cifs_options.domain,
				   pam_cifs_options.options, authtok, 0,
				   0, user, pam_cifs_options.background);
	    }
	} while((mount_count++ <= 3) && (mount_error < 0));
	
	if (mount_error < 0) {
	    slog("looking form trailing path components...\n");
	    char delim[2] = "/";
	    delim[0] = *source; // the first char must be the delimiter
	    
	    if ((s = rindex(source, delim[0])) != NULL) {
		
		slog("stripping %s\n", (s+1));
		*s = '\0'; // strip off the last component in source
		strncat(strippedpathcomponents, "/",
			sizeof(strippedpathcomponents) -strlen(strippedpathcomponents) - 1);
		strncat(strippedpathcomponents, (s+1),
			sizeof(strippedpathcomponents) -strlen(strippedpathcomponents) - 1);
	    }
	    else {
		return cleanup(PAM_SESSION_ERR);
	    }
	}
	else {
	    // this obviously only works if backgrounding is off
	    break;
	}
	
    }while(1==1);

    // the target is modified because the link has to point to th
    // stripped path components
    strncat(target, strippedpathcomponents, sizeof(target) - strlen(target) - 1);
    
    /*
    if (do_mount(source, target, pam_cifs_options.domain,
		 pam_cifs_options.options, authtok, pwd->pw_uid,
		 pwd->pw_gid, user, pam_cifs_options.background) < 0) {
	return cleanup(PAM_SESSION_ERR);
    }
    */
    if (strncmp(pam_cifs_options.linkname, "", PATH_MAX) != 0) {
	char linkpath[PATH_MAX];
	strncpy(linkpath, pwd->pw_dir, sizeof(linkpath));
	strncat(linkpath, "/", sizeof(linkpath)-strlen(linkpath)-1);
	strncat(linkpath, pam_cifs_options.linkname, sizeof(linkpath)-strlen(linkpath)-1);
	slog("should create link %s to %s\n", linkpath, target);
	if (symlink(target, linkpath) < 0) {
	    slog("symlink: %s\n", strerror(errno));
	    return cleanup(PAM_SESSION_ERR);
	}
    }
    
    return cleanup(PAM_SUCCESS);
}

