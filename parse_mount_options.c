/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/parse_mount_options.c,v 1.2 2005/08/29 11:33:42 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs.h"
#include "cifsumountd.h"

#include <regex.h>
#include <strings.h>

extern struct options pam_cifs_options;

static int isInitialized = 0;

static void c_free();

static int init() {
    if (isInitialized == 0) {		
	isInitialized = 1;
    }
    return 0;
}

static int cleanup(int error) {
    int errno_save = errno;

    if (isInitialized > 0) {
    }
    c_free();
    isInitialized = 0;
    errno = errno_save;
    return error;
}

static void **mlist = NULL;
static int  n_mlist = 0;

static void c_free() {
    if (mlist != NULL) {
	for(int i = 0; i < n_mlist; i += 1) {
	    if (mlist[i] != NULL) {
		free(mlist[i]);
	    }
	}
	free(mlist);
	mlist = NULL;
	n_mlist = 0;
    }
}

static void *c_add(void *ptr) {
    mlist = (void **) realloc(mlist, (n_mlist + 1) * sizeof(void **));
    mlist[n_mlist] = ptr;
    n_mlist += 1;
    return ptr;
}

//static void *c_malloc(size_t size) {
//    return c_add(malloc(size));
//}

char *get_mount_option_value(const char *key, mopts_t *mopts, size_t nopts) {
    if (mopts == NULL) {
	return NULL;
    }
    for(int i = 0; i < nopts; i+= 1) {
	if (strcmp(mopts[i].key, key) == 0) {
	    return mopts[i].value;
	}
    }
    return NULL;
}


int parse_mount_options(const char *options, mopts_t *mopts, size_t max_options) {
    if (init() < 0) {
	return cleanup(-1);
    }
    char *o_dup = c_add(strdup(options));
    char *token;
    char *oo = o_dup;

    int nopts = 0;
    char *ooo = NULL;
    while(((token = strtok_r(oo, CIFS_ODELIM, &ooo)) != NULL) && (nopts < max_options)) {
	oo = NULL;
	
	char *t_dup = c_add(strdup(token));

	char *ttt = NULL;
	char *key = strtok_r(t_dup, CIFS_VDELIM, &ttt);
	char *value = strtok_r(NULL, CIFS_VDELIM, &ttt);
		
	if (key != NULL) {
	    strcpy(mopts[nopts].key, key);
	}
	else {
	    mopts[nopts].key[0] = '\0';
	}
	if (value != NULL) {
	    strcpy(mopts[nopts].value, value);
	}
	else {
	    mopts[nopts].value[0] = '\0';
	}
	nopts += 1;
    }
    return cleanup(nopts);
}


