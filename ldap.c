/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/ldap.c,v 1.6 2008/01/15 20:39:33 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs_module.h"
#define USE_C_CLEANUP_P
#include "cleanup.h"

#define LDAP_DEPRECATED 1
#include <ldap.h>

#include "pam_ldap.h"

#include <string.h>
#include <assert.h>

extern struct options pam_cifs_options;

static LDAP *ld = NULL;
static LDAPMessage* msg = NULL;
static BerElement* ber = NULL;

void ldap_cleanup() {
    if (ber != NULL) {
	ber_free(ber, 0);
    }
    ber = NULL;
    
    if ((msg != NULL) && (ldap_msgfree(msg) < 0)) {
	slog("ldap unbind failure: %s\n", strerror(errno));
    }
    msg = NULL;
	
    if ((ld != NULL) && (ldap_unbind_s(ld) < 0)) {
	slog("ldap unbind failure: %s\n", strerror(errno));
    }
    ld = NULL;
}

char* ldap_source(pam_handle_t* pamh, const char* user, const char* attribute) {
   int opt_value;
   pam_ldap_session_t *pam_ldap_session = NULL;
   char filter[PATH_MAX];
   char *base;
   
   init(NULL, ldap_cleanup);

#ifdef NO_PAM_MODULE
#else
   int retval;

   if (pamh != NULL) {
       if ((retval = pam_get_data(pamh, PADL_LDAP_SESSION_DATA,
				  (const void **)&pam_ldap_session)) != PAM_SUCCESS) {
	   slog("can't get pam_ldap data\n");
       }
       else {
	   if (pam_ldap_session != NULL) {
	       slog("pam_ldap uses: %s, %s\n", pam_ldap_session->conf->binddn,
		    pam_ldap_session->conf->bindpw);
	       if (pam_ldap_session->ld != NULL) {
		   ld = pam_ldap_session->ld;
	       }
	   }
	   else {
	       slog("no pam_ldap Session data valid\n");
	   }
       }
   }
#endif
   
   if (ld == NULL) {
       slog("setting up ldap connection\n");
       // LDAP global options
       opt_value = 0;
       if( ldap_set_option(NULL, LDAP_OPT_DEBUG_LEVEL, &opt_value) != LDAP_OPT_SUCCESS) {
	   slog("Could not set LDAP_OPT_DEBUG_LEVEL %d\n", opt_value);
       }	
   
       opt_value = LDAP_OPT_X_TLS_ALLOW;
       if (ldap_set_option(NULL, LDAP_OPT_X_TLS_REQUIRE_CERT, &opt_value) != LDAP_OPT_SUCCESS) {
	   slog("Could not set LDAP_OPT_X_TLS_REQUIRE_CERT\n");
	   return cleanup_p(NULL);
       }	

       if (strlen(pam_cifs_options.ldap) == 0) {
	   slog("No ldap server specified\n");
	   return cleanup_p(NULL);
       }
   
       if (ldap_initialize(&ld, pam_cifs_options.ldap) != LDAP_SUCCESS) {
	   slog("Could not initialize ldap: %s : %s\n", pam_cifs_options.ldap, strerror(errno));
	   return cleanup_p(NULL);
       }

       // LDAP conection specific options
       opt_value = LDAP_VERSION3;
       if (ldap_set_option(ld, LDAP_OPT_PROTOCOL_VERSION, &opt_value) != LDAP_OPT_SUCCESS) {
	   slog("Could not set protocol version\n");
	   return cleanup_p(NULL);
       }
  
       if (ldap_set_option(ld, LDAP_OPT_REFERRALS, LDAP_OPT_OFF) != LDAP_OPT_SUCCESS) {
	   slog("Could not deactives referrals\n");
	   return cleanup_p(NULL);
       }

       opt_value = 1;
       if (ldap_set_option(ld, LDAP_OPT_SIZELIMIT, &opt_value) != LDAP_OPT_SUCCESS) {
	   slog("Could not set sizelimit\n");
	   return cleanup_p(NULL);
       }

       if (strlen(pam_cifs_options.ldapbinddn) == 0) {
	   slog("No ldap binddn specified\n");
	   return cleanup_p(NULL);
       }
       if (strlen(pam_cifs_options.ldapbindpw) == 0) {
	   slog("No ldap bindpw specified\n");
	   return cleanup_p(NULL);
       }
   
       if (ldap_simple_bind_s(ld, pam_cifs_options.ldapbinddn,
			      pam_cifs_options.ldapbindpw) != LDAP_SUCCESS) {
	   slog("Could not bind to ldap server\n");
	   return cleanup_p(NULL);
       }

       if (strlen(pam_cifs_options.ldaploginattribute) == 0) {
	   slog("No ldap loginattribute specified\n");
	   return cleanup_p(NULL);
       }
       if (strlen(pam_cifs_options.ldapbasedn) == 0) {
	   slog("No ldap basedn specified\n");
	   return cleanup_p(NULL);
       }
       base = pam_cifs_options.ldapbasedn;
       
       snprintf(filter, PATH_MAX, "(&(ObjectClass=%s)(%s=%s))",
		pam_cifs_options.ldapobjectclass,
		pam_cifs_options.ldaploginattribute, user);
   }
   else {
       slog("using pam_ldap_connection\n");
       assert(pam_ldap_session != NULL);
       snprintf(filter, PATH_MAX, "(&(ObjectClass=%s)(%s=%s))",
		pam_cifs_options.ldapobjectclass,
		pam_ldap_session->conf->userattr, user);
       base = pam_ldap_session->conf->base;
   }	
   
   const char* attrs[2];
   attrs[0] = attribute;
   attrs[1] = NULL;

   if (ldap_search_s(ld, base, LDAP_SCOPE_SUBTREE,
		     filter, (char **) attrs, 0, &msg) != LDAP_SUCCESS) {
       slog("Could not ldap search: %s\n", ldap_err2string(ldap_result2error(ld, msg, 1)));
       return cleanup_p(NULL);
   }	
      
   if (ldap_count_entries(ld, msg) > 1) {
       slog("Got more than 1 ldap entry ... strange\n");
   }

   LDAPMessage* entry;
   if ((entry = ldap_first_entry(ld, msg)) == NULL) {
       slog("No ldap entry found: user: %s\n", user);
       return cleanup_p(NULL);
   }
   else {
       slog("Got ldap entry for %s\n", user);
   }

   for(char* attr = ldap_first_attribute(ld, entry, &ber); attr != NULL;
       attr = ldap_next_attribute(ld, entry, ber)) {
       if (strncmp(attr, attribute, strlen(attribute)) == 0) { // found
	   slog("got ldap attribute %s\n", attr);
	   char** values;
	   if ((values = ldap_get_values(ld, entry, attr)) != NULL) { // has values
	       if (values[0] != NULL) { // get the first value if
					// valid
		   char* res = strdup(values[0]);
		   slog("got ldap attribute %s, value %s\n", res);
		   ldap_value_free(values);
		   ldap_memfree(attr);
		   return cleanup_p(res);
	       }
	   }
	   ldap_value_free(values);
       }
       ldap_memfree(attr);
   }
   return cleanup_p(NULL);
}







