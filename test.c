/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/test.c,v 1.1 2007/09/09 10:01:16 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs_module.h"

#include <string.h>

char* testuser = NULL;
char* testauthtok = NULL;

extern struct options pam_cifs_options;

// only a small test
int main( int argc, const char *argv[] ) {
    if ((testuser = getenv("USER")) == NULL) {
	printf("no env varibale USER\n");
	exit(EXIT_FAILURE);
    }

    if ((testauthtok = getenv("PASSWORD")) == NULL) {
	printf("no env varibale PASSWORD\n");
	exit(EXIT_FAILURE);
    }

    int ret = pam_sm_open_session(NULL, 0, argc, argv);

    exit(ret);
}
