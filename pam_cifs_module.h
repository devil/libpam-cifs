/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/pam_cifs_module.h,v 1.3 2007/08/23 12:56:00 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef PAM_CIFS_MODULE_H
#define PAM_CIFS_MODULE_H

#include "pam_cifs.h"

#define PAM_SM_AUTH
#define PAM_SM_ACCOUNT
#define PAM_SM_SESSION
#define PAM_SM_PASSWORD

#include <security/pam_modules.h>
#include <security/_pam_macros.h>

#define PAM_CIFS_AUTHTOK "pam_cifs_authtok"

// from pam_ldap.h
#define PADL_LDAP_SESSION_DATA "PADL-LDAP-SESSION-DATA"

char* ldap_source(pam_handle_t *pamh, const char* user, const char* attribute);


#endif
