/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/do_mount.c,v 1.7 2007/09/25 12:04:41 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs_module.h"
#include <regex.h>

int
do_mount(char *source, char *target, char *domain, char *options, const char *authtok,
	 uid_t uid, gid_t gid, const char *user, int background) {
    slog("do_mount: %s should be mounted on %s", source, target);

    char *argv[10];
    argv[0] = (char *) malloc(NAME_MAX + 1);
    strncpy(argv[0], "pam_cifs_helper", NAME_MAX);
    argv[1] = source;
    argv[2] = target;
    argv[3] = "-o";
    argv[4] = (char *) malloc(LINE_MAX);
    if (strncmp(options, "", NAME_MAX) == 0) {
	if (strncmp(domain, "", NAME_MAX) == 0) {
	    if ((uid > 0) && (gid > 0)) {
	       snprintf(argv[4], LINE_MAX, "user=%s,uid=%d,gid=%d,dir_mode=0700,file_mode=0600",
		     user, uid, gid);
            }
            else {
	       snprintf(argv[4], LINE_MAX, "user=%s,dir_mode=0700,file_mode=0600",
		     user);
            }
	}
	else {
	    if ((uid > 0) && (gid > 0)) {
	        snprintf(argv[4], LINE_MAX, "user=%s,uid=%d,gid=%d,dir_mode=0700,file_mode=0600,domain=%s",
		     user, uid, gid, domain);
	    }
            else {
	        snprintf(argv[4], LINE_MAX, "user=%s,dir_mode=0700,file_mode=0600,domain=%s",
		     user, domain);
	    }
	}
    }
    else {
	if (strncmp(domain, "", NAME_MAX) == 0) {
	    if ((uid > 0) && (gid > 0)) {
	        snprintf(argv[4], LINE_MAX, "%s,user=%s,uid=%d,gid=%d,dir_mode=0700,file_mode=0600",
		     options, user, uid, gid);
  	    }
	    else {
	        snprintf(argv[4], LINE_MAX, "%s,user=%s,dir_mode=0700,file_mode=0600",
		     options, user);
  	    }
	}
	else {
	    if ((uid > 0) && (gid > 0)) {
	       snprintf(argv[4], LINE_MAX, "%s,user=%s,uid=%d,gid=%d,dir_mode=0700,file_mode=0600,domain=%s",
		     options, user, uid, gid, domain);
	    }
	    else {
	       snprintf(argv[4], LINE_MAX, "%s,user=%s,dir_mode=0700,file_mode=0600,domain=%s",
		     options, user, domain);
	    }
	}
    }
    argv[5] = NULL;

    slog("do_mount: mountoptions: %s", argv[4]);

    char ret[LINE_MAX];

    char input[LINE_MAX];
    snprintf(input, LINE_MAX, "%s\n", authtok);
    
    if (setuid(0) < 0) {
	slog("setuid: %s", strerror(errno));
    }
    int retval = 0;
    if (pexec(DEFAULT_MOUNTER, argv, background, input, ret, LINE_MAX) < 0) {
	retval = -1;
    }
    free(argv[0]);
    free(argv[4]);

    // checking for "error" in the return line of mount.cifs

    char* regex = ".*error.*";
    regex_t cregex;
    if (regcomp(&cregex, regex, REG_EXTENDED | REG_NOSUB) < 0) {
	slog("regcomp failure");
	return -1;
    }
    if (regexec(&cregex, ret, 0, NULL, 0) == 0) {
	slog("got an error in mounthelper reply");
	retval = -1;
    }
    regfree(&cregex);
    
    return retval;
}
