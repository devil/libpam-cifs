/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/read_mounts.c,v 1.2 2005/08/29 11:33:42 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs.h"

int
read_mounts(mtab_t *mtab, size_t size) {
    FILE *mf;

    if ((mf = fopen(MOUNTS, "r")) == NULL) {
	slog("fopen %s: %s", MOUNTS, strerror(errno));
	return -1;
    }
    char line[LINE_MAX], *r;
    int i = 0;
    while (((r = fgets(line, LINE_MAX, mf)) != NULL) && (i < size)) {
	strncpy(mtab[i].source, strtok(line, " \t"), PATH_MAX); 
	strncpy(mtab[i].target, strtok(NULL, " \t"), PATH_MAX);
	strncpy(mtab[i].type, strtok(NULL, " \t"), PATH_MAX);
	strncpy(mtab[i].options, strtok(NULL, " \t"), PATH_MAX);
	i += 1;
    }
    fclose(mf);
    return i;
}
