/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/cifs_check.c,v 1.4 2007/08/23 11:31:12 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs.h"
#include "cifsumountd.h"
#define USE_C_MALLOC
#define USE_C_CLEANUP
#include "cleanup.h"

#include <regex.h>

extern struct options pam_cifs_options;

static regex_t cregex;

static int localinit() {
    char regex[PATH_MAX];
    snprintf(regex, PATH_MAX, "%s/.*%s", pam_cifs_options.pathprefix,
	     pam_cifs_options.pathsuffix);
    if (regcomp(&cregex, regex, REG_EXTENDED | REG_NOSUB) < 0) {
	slog("regcomp failure");
	return -1;
    }
    return 0;
}

static void localcleanup(int error) {
    regfree(&cregex);
}

int cifs_check() {
    mtab_t *mtab;
    if (init(localinit, localcleanup) < 0) {
	return cleanup(-1);
    }
    if ((mtab = (mtab_t *) c_malloc(sizeof(mtab_t) * MAX_MTAB)) == NULL) {
	slog("malloc: %s", strerror(errno));
	return -1;
    }

    int n_mounts = read_mounts(mtab, MAX_MTAB);
    if (n_mounts < 0) {
	slog("read_mounts: %s", strerror(errno));
	return cleanup(-1);
    }

    for(int i = 0; i < n_mounts; i += 1) {
	if (strcmp(mtab[i].type, CIFS_STRING) == 0) {
	    slog("consider mtab entry: %s\n", mtab[i].target);
	    if (regexec(&cregex, mtab[i].target, 0, NULL, 0) == 0) {
		slog("matches regex\n");
		mopts_t *mopts = c_malloc(MAX_OPTIONS * sizeof(mopts_t));
		int nopts = parse_mount_options(mtab[i].options, mopts, MAX_OPTIONS);
		char *user = get_mount_option_value(CIFS_USERNAME, mopts, nopts);
		if (user != NULL) {
		    // check if a user process exists
		    struct passwd *pwd = getpwnam(user);
		    if (pwd != NULL) {
			if ((pwd->pw_uid < pam_cifs_options.min_uid) ||
			    (pwd->pw_uid > pam_cifs_options.max_uid)) {
			    slog("uid out of range\n");
			    continue;
			}
			if (user_process(pwd->pw_uid) == 0) {
			    slog("user %s mounted %s, no userprocess -> umount",
				 user, mtab[i].target);
			    char c[PATH_MAX];
			    snprintf(c, PATH_MAX, "%s %s", UMOUNT, mtab[i].target);
			    FILE *f = popen(c, "r");
			    if (f != NULL) {
				char line[LINE_MAX];
				while((fgets(line, LINE_MAX, f)) != NULL) {
				    slog("umount: %s", line);
				}
				pclose(f);
			    }
			}
		    }
		}
	    }
	}
    }
    return cleanup(0);
}


