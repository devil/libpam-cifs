/*
 * $Header: /cvsroot/pam-cifs/pam-cifs/already_mounted.c,v 1.3 2007/08/23 07:10:10 wimalopaan Exp $
 *
 *  pam_cifs - Linux-PAM module for mount/umount CIFS shares
 *  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "pam_cifs_module.h"
#define USE_C_MALLOC
#define USE_C_CLEANUP
#include "cleanup.h"

extern struct options pam_cifs_options;

static mtab_t *mtab;

int
already_mounted(char *path) {
    if (init(NULL, NULL) < 0) {
	return cleanup(-1);
    }

    if ((mtab = (mtab_t *) c_malloc(sizeof(mtab_t) * MAX_MTAB)) == NULL) {
	slog("malloc: %s", strerror(errno));
	return -1;
    }
    
    int n;    
    if ((n = read_mounts(mtab, MAX_MTAB)) < 0) {
	return cleanup(-1);
    }
    for(int i = 0; i < n; i += 1) {
	if (strcmp(mtab[i].type, "cifs") == 0) {
	    if (strcmp(mtab[i].target, path) == 0) {
		slog("%s is already mounted", path);
		return cleanup(1);
	    }
	}
    }
    return cleanup(0);
}
