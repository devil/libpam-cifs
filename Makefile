#  $Header: /cvsroot/pam-cifs/pam-cifs/Makefile,v 1.21 2008/01/15 20:48:28 wimalopaan Exp $
#
#  pam_cifs - Linux-PAM module for mount/umount CIFS shares
#
#  Copyright (C) 2005  Wilhelm Meier (meier@informatik.fh-kl.de)
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#

CC=gcc
OPTFLAGS = -O2
CFLAGS += $(OPTFLAGS) -g -Wall -pedantic -std=c99 -fPIC -fno-strict-aliasing

CFLAGS += -DDEFAULT_PATH_PREFIX='"/Home"'
CFLAGS += -DDEFAULT_PATH_SUFFIX='""'
CFLAGS += -DDEFAULT_SOURCE_PREFIX='""'
CFLAGS += -DDEFAULT_SOURCE_SUFFIX='""'
CFLAGS += -DDEFAULT_MOUNTER='"/sbin/mount.cifs"'
CFLAGS += -DDEFAULT_MIN_UID=1000
CFLAGS += -DDEFAULT_MAX_UID=65000
CFLAGS += -DDEFAULT_LINK_NAME='""'
CFLAGS += -DDEFAULT_DOMAIN='"Workgroup"'
CFLAGS += -DDEFAULT_BACKGROUND=1

ifndef PAM_CIFS_WITHOUT_LDAP
CFLAGS += -DWITH_LDAP
LDLIBS += -lldap -llber

# mount.cifs is patched (return value)
# use this for testing purpose
# or install it over the original mount.cifs at your own risk!

all: cifsumountd pam_cifs.so ldaptest test mount.cifs

# ldaptest is a small test-program for connecting to an ldap-server
# and getting the attributes

ldaptest: ldaptest.o ldapnm.o slog_nm.o parse_options.o 

ldapnm.o: ldap.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) -DNO_PAM_MODULE $< -o $@

test: test.o ldapnm.o read_mounts.o slog.o parse_options.o pam_sm_open_session_nm.o already_mounted.o do_mount.o pexec.o mkdirpath.o

pam_cifs.so: pam_cifs.o read_mounts.o slog.o parse_options.o pam_sm_open_session.o already_mounted.o do_mount.o pexec.o mkdirpath.o ldap.o
	$(CC) -shared -WL,-soname,$@ -o $@ $^  $(LDLIBS)

else

all: cifsumountd pam_cifs.so test mount.cifs

pam_cifs.so: pam_cifs.o read_mounts.o slog.o parse_options.o pam_sm_open_session.o already_mounted.o do_mount.o pexec.o mkdirpath.o
	$(CC) -shared -WL,-soname,$@ -o $@ $^  $(LDLIBS)

test: test.o read_mounts.o slog_nm.o parse_options.o pam_sm_open_session_nm.o already_mounted.o do_mount.o pexec.o mkdirpath.o

endif

mount.cifs: mount.cifs.c

pam_sm_open_session_nm.o: pam_sm_open_session.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) -DNO_PAM_MODULE $< -o $@

slog_nm.o: slog.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) -DNO_PAM_MODULE $< -o $@

cifsumountd: cifsumountd.o read_mounts.o slog.o parse_options.o cifs_check.o daemonize.o parse_mount_options.o

cifsumountd.o: cifsumountd.c common.h pam_cifs.h cifsumountd.h cleanup.h

cifs_check.o: cifs_check.c common.h pam_cifs.h cifsumountd.h cleanup.h

slog.o: slog.c common.h pam_cifs.h cleanup.h

daemonize.o: daemonize.c common.h cleanup.h

parse_options.o: parse_options.c common.h pam_cifs.h cleanup.h

parse_mount_options.o: parse_mount_options.c common.h pam_cifs.h cifsumountd.h cleanup.h

pam_cifs.o: pam_cifs.c common.h pam_cifs.h pam_cifs_module.h cleanup.h

pam_sm_open_session.o: pam_sm_open_session.c common.h pam_cifs.h pam_cifs_module.h cleanup.h

pexec.o: pexec.c common.h pam_cifs.h cleanup.h

read_mounts.o: read_mounts.c common.h pam_cifs.h cleanup.h

already_mounted.o: already_mounted.c common.h pam_cifs.h cleanup.h

do_mount.o: do_mount.c common.h pam_cifs.h cleanup.h

mkdirpath.o: mkdirpath.c common.h pam_cifs.h cleanup.h

install: all
	cp pam_cifs.so $(DESTDIR)/lib/security
	cp cifsumountd $(DESTDIR)/usr/sbin
	cp cifsumount $(DESTDIR)/etc/init.d

clean:
	$(RM) *.o *.so *~ cifsumountd ldaptest test mount.cifs
